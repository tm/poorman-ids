sudo apt-get install freebsd-buildutils mailutils -y
sudo apt-get autoremove -y
sudo git clone https://gitlab.msu.edu/tm/poorman-ids.git /ids
sudo cp /ids/_config.sh /ids/config.sh
sudo chmod -R 700 /ids
sudo nano /ids/config.sh
echo ""
echo "Generating initial filesystem fingerprint"
echo ""
sudo /ids/ids.sh generate

echo ""
echo "You should add a cron task to automatically run the verify"
echo "process on a regular basis and to send you an email when"
echo "when anything changes.  You can open the cron scheduler"
echo "using the command:"
echo ""
echo "sudo crontab -e "
echo ""
echo "You can then add the following on a new line within the editor"
echo "00   */1  *   *   *   /ids/ids.sh verify"
echo ""