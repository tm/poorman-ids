#!/bin/bash
#
## Based on Calomel.org  ids.sh
#

if [ $# -eq 0 ]
   then
    echo ""
    echo "--------------------------------------"
    echo "generate = generate IDS signatures"
    echo "verify   = verify files against known signatures"
    echo "--------------------------------------"
    echo ""
   exit
 fi

## read in the variables from the config.sh file
source /ids/config.sh

## mtree binary (OpenBSD: mtree and Linux: freebsd-mtree)
MTREE=freebsd-mtree 

## IDS signature directory
DIR=/ids

if [ $1 = "generate" ]
   then
     rm -rf $DIR/mtree_*
     cd $DIR
     $MTREE -c -K cksum -s $KEY -p /bin > mtree_bin
     $MTREE -c -K cksum -s $KEY -p /sbin > mtree_sbin
     $MTREE -c -K cksum -s $KEY -p /usr > mtree_usr
     $MTREE -c -K cksum -s $KEY -p /etc > mtree_etc
     logger IDS generate IDS signatures
     chmod 600 $DIR/mtree_*
   exit
 fi

if [ $1 = "verify" ]
   then
     cd $DIR
     $MTREE -s $KEY -p /bin < mtree_bin >> temp 2>&1
     $MTREE -s $KEY -p /sbin < mtree_sbin >> temp 2>&1
     $MTREE -s $KEY -p /usr < mtree_usr >> temp 2>&1
     $MTREE -s $KEY -p /etc < mtree_etc >> temp 2>&1
     cat temp | mail -s "`hostname` file integrity check" $EMAIL_ADDRESS
     rm temp
     logger IDS verify files against known signatures
   exit
 fi
