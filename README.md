# Poorman's Intrusion Detection System

This project provides a very basic and inexpensive way
to determine if the files on your server have been
changed without your knowledge.  The system generates
a checksum for the directories that are defined in
the ids.sh file and can be verified by the system
on a scheduled basis through the use of a cron job.

This script has been tested on Ubuntu Server 12.04.

Based on the script and directions found at https://calomel.org/ids_mtree.html

## HOW TO USE

You need to have the binary called mtree (OpenBSD or FreeBSD) or freebsd-mtree 
(Ubuntu linux). Mtree is installed on OpenBSD and FreeBSD by default. If you 
are running Ubuntu Linux just install the 
package, "sudo apt-get install freebsd-buildutils".

To use this tool run the commands in the install.sh on
your Ubuntu server or run the script itself using 'sudo install.sh'

When the nano text editor appears paste in the contents of the ids.sh
file, making sure to change the KEY variable to a random 23 digit
number that will be used with the fingerprint.  You **MUST** retain
this KEY value to be able to accurately verify that the file contents
have not changed.